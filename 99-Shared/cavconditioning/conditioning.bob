<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-07-11 16:43:06 by muyuanwang-->
<display version="2.0.0">
  <name>$(C) Conditioning</name>
  <width>1740</width>
  <height>1120</height>
  <widget type="rectangle" version="2.0.0">
    <name>BGGrey01-background</name>
    <x>10</x>
    <y>60</y>
    <width>1720</width>
    <height>1060</height>
    <line_width>1</line_width>
    <line_color>
      <color red="175" green="175" blue="175">
      </color>
    </line_color>
    <background_color>
      <color name="Background" red="220" green="225" blue="221">
      </color>
    </background_color>
    <corner_width>3</corner_width>
    <corner_height>3</corner_height>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>BGGrey01-background</name>
    <x>780</x>
    <y>70</y>
    <width>940</width>
    <height>1040</height>
    <line_width>2</line_width>
    <line_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </line_color>
    <background_color>
      <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
      </color>
    </background_color>
    <corner_width>10</corner_width>
    <corner_height>10</corner_height>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>BGGrey01-background</name>
    <x>20</x>
    <y>70</y>
    <width>750</width>
    <height>1040</height>
    <line_width>2</line_width>
    <line_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </line_color>
    <background_color>
      <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
      </color>
    </background_color>
    <corner_width>10</corner_width>
    <corner_height>10</corner_height>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Embedded Display</name>
    <macros>
      <PI_TYPE>SP</PI_TYPE>
    </macros>
    <file>spTbl.bob</file>
    <x>790</x>
    <y>730</y>
    <width>240</width>
    <height>370</height>
    <border_width>2</border_width>
    <border_color>
      <color name="GRAY-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Embedded Display</name>
    <macros>
      <PI_TYPE>FF</PI_TYPE>
    </macros>
    <file>ffTbl.bob</file>
    <x>1040</x>
    <y>730</y>
    <width>240</width>
    <height>370</height>
    <border_width>2</border_width>
    <border_color>
      <color name="GRAY-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Embedded Display</name>
    <file>parameters.bob</file>
    <x>1290</x>
    <y>730</y>
    <width>420</width>
    <height>370</height>
    <border_width>2</border_width>
    <border_color>
      <color name="GRAY-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Embedded Display</name>
    <file>radiation.bob</file>
    <x>1050</x>
    <y>80</y>
    <width>660</width>
    <height>420</height>
    <border_width>2</border_width>
    <border_color>
      <color name="GRAY-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Embedded Display</name>
    <file>llrf.bob</file>
    <x>790</x>
    <y>80</y>
    <width>250</width>
    <height>420</height>
    <border_width>2</border_width>
    <border_color>
      <color name="GRAY-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Embedded Display</name>
    <file>hvbias.bob</file>
    <x>1370</x>
    <y>510</y>
    <width>340</width>
    <height>210</height>
    <border_width>2</border_width>
    <border_color>
      <color name="GRAY-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>Titlebar</name>
    <width>1740</width>
    <height>50</height>
    <line_width>0</line_width>
    <background_color>
      <color name="PRIMARY-HEADER-BACKGROUND" red="151" green="188" blue="202">
      </color>
    </background_color>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>Timestamp</name>
    <pv_name>TS2-010:Ctrl-EVM-101:Timestamp-RB</pv_name>
    <x>1430</x>
    <y>10</y>
    <width>300</width>
    <height>30</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="20.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <border_width>2</border_width>
    <border_color>
      <color name="GRAY-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="xyplot" version="3.0.0">
    <name>X/Y Plot</name>
    <x>30</x>
    <y>120</y>
    <width>360</width>
    <height>280</height>
    <show_legend>false</show_legend>
    <scripts>
      <script file="EmbeddedPy" check_connections="false">
        <text><![CDATA[# Embedded python script
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil
import math

# cavity gradient enable
enable=PVUtil.getDouble(pvs[0])
# cold cable attenuator
Att=PVUtil.getDouble(pvs[1])
att=1/(math.pow(10,Att/10))

if enable==0:
	widget.setPropertyValue('y_axes[0].title', u"Power [W]")
	widget.setPropertyValue('traces[0].y_pv', "=arrayDivScalar('"+str(pvs[2])+"',"+str(att)+")")
elif enable==1:
	widget.setPropertyValue('y_axes[0].title', u"Eacc [MV/m]")
	widget.setPropertyValue('traces[0].y_pv', pvs[2])]]></text>
        <pv_name>$(PD)$(RD)AI0-CalGradEn</pv_name>
        <pv_name>$(PD)$(RD)AI0-CalGradA</pv_name>
        <pv_name trigger="false">$(PD)$(RD)Dwn$(CAVITYCH=0)-Cmp0.{"arr":{"s":0,"i":100,"e":-1}}</pv_name>
      </script>
    </scripts>
    <tooltip></tooltip>
    <x_axis>
      <title>Time [ms]</title>
      <autoscale>true</autoscale>
      <log_scale>false</log_scale>
      <minimum>0.0</minimum>
      <maximum>10.0</maximum>
      <show_grid>true</show_grid>
      <title_font>
        <font family="Source Sans Pro" style="REGULAR" size="16.0">
        </font>
      </title_font>
      <scale_font>
        <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
        </font>
      </scale_font>
      <visible>true</visible>
    </x_axis>
    <y_axes>
      <y_axis>
        <title></title>
        <autoscale>true</autoscale>
        <log_scale>false</log_scale>
        <minimum>0.0</minimum>
        <maximum>10.0</maximum>
        <show_grid>true</show_grid>
        <title_font>
          <font family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </title_font>
        <scale_font>
          <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </scale_font>
        <on_right>false</on_right>
        <visible>true</visible>
        <color>
          <color name="Text" red="25" green="25" blue="25">
          </color>
        </color>
      </y_axis>
    </y_axes>
    <traces>
      <trace>
        <name></name>
        <x_pv>$(PD)$(RD)Dwn0-XAxis.{"arr":{"s":0,"i":100,"e":-1}}</x_pv>
        <y_pv></y_pv>
        <err_pv></err_pv>
        <axis>0</axis>
        <trace_type>1</trace_type>
        <color>
          <color red="0" green="0" blue="255">
          </color>
        </color>
        <line_width>2</line_width>
        <line_style>0</line_style>
        <point_type>0</point_type>
        <point_size>10</point_size>
        <visible>true</visible>
      </trace>
    </traces>
  </widget>
  <widget type="xyplot" version="3.0.0">
    <name>X/Y Plot</name>
    <x>30</x>
    <y>450</y>
    <width>360</width>
    <height>280</height>
    <tooltip></tooltip>
    <x_axis>
      <title>Time [ms]</title>
      <autoscale>true</autoscale>
      <log_scale>false</log_scale>
      <minimum>0.0</minimum>
      <maximum>10.0</maximum>
      <show_grid>true</show_grid>
      <title_font>
        <font family="Source Sans Pro" style="REGULAR" size="16.0">
        </font>
      </title_font>
      <scale_font>
        <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
        </font>
      </scale_font>
      <visible>true</visible>
    </x_axis>
    <y_axes>
      <y_axis>
        <title>Power [kw]</title>
        <autoscale>true</autoscale>
        <log_scale>false</log_scale>
        <minimum>0.0</minimum>
        <maximum>10.0</maximum>
        <show_grid>true</show_grid>
        <title_font>
          <font family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </title_font>
        <scale_font>
          <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </scale_font>
        <on_right>false</on_right>
        <visible>true</visible>
        <color>
          <color name="Text" red="25" green="25" blue="25">
          </color>
        </color>
      </y_axis>
    </y_axes>
    <traces>
      <trace>
        <name>Pfwd</name>
        <x_pv>$(PD)$(RD)Dwn5-XAxis.{"arr":{"s":0,"i":100,"e":-1}}</x_pv>
        <y_pv>$(PD)$(RD)Dwn5-Cmp0.{"arr":{"s":0,"i":100,"e":-1}}</y_pv>
        <err_pv></err_pv>
        <axis>0</axis>
        <trace_type>1</trace_type>
        <color>
          <color red="0" green="0" blue="255">
          </color>
        </color>
        <line_width>2</line_width>
        <line_style>0</line_style>
        <point_type>0</point_type>
        <point_size>10</point_size>
        <visible>true</visible>
      </trace>
      <trace>
        <name>Prefl</name>
        <x_pv>$(PD)$(RD)Dwn6-XAxis.{"arr":{"s":0,"i":100,"e":-1}}</x_pv>
        <y_pv>$(PD)$(RD)Dwn6-Cmp0.{"arr":{"s":0,"i":100,"e":-1}}</y_pv>
        <err_pv></err_pv>
        <axis>0</axis>
        <trace_type>1</trace_type>
        <color>
          <color red="255" green="0" blue="0">
          </color>
        </color>
        <line_width>2</line_width>
        <line_style>0</line_style>
        <point_type>0</point_type>
        <point_size>10</point_size>
        <visible>true</visible>
      </trace>
      <trace>
        <name>Pkly</name>
        <x_pv>$(PD)$(RD)Dwn3-XAxis.{"arr":{"s":0,"i":100,"e":-1}}</x_pv>
        <y_pv>$(PD)$(RD)Dwn3-Cmp0.{"arr":{"s":0,"i":100,"e":-1}}</y_pv>
        <err_pv></err_pv>
        <axis>0</axis>
        <trace_type>1</trace_type>
        <color>
          <color red="0" green="128" blue="128">
          </color>
        </color>
        <line_width>2</line_width>
        <line_style>0</line_style>
        <point_type>0</point_type>
        <point_size>10</point_size>
        <visible>true</visible>
      </trace>
    </traces>
  </widget>
  <widget type="xyplot" version="3.0.0">
    <name>X/Y Plot</name>
    <x>30</x>
    <y>780</y>
    <width>730</width>
    <height>320</height>
    <show_toolbar>true</show_toolbar>
    <x_axis>
      <title>Time [ms]</title>
      <autoscale>false</autoscale>
      <log_scale>false</log_scale>
      <minimum>0.35</minimum>
      <maximum>3.18</maximum>
      <show_grid>true</show_grid>
      <title_font>
        <font family="Source Sans Pro" style="REGULAR" size="16.0">
        </font>
      </title_font>
      <scale_font>
        <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
        </font>
      </scale_font>
      <visible>true</visible>
    </x_axis>
    <y_axes>
      <y_axis>
        <title>f [Hz]</title>
        <autoscale>true</autoscale>
        <log_scale>false</log_scale>
        <minimum>0.0</minimum>
        <maximum>10.0</maximum>
        <show_grid>true</show_grid>
        <title_font>
          <font family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </title_font>
        <scale_font>
          <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </scale_font>
        <on_right>false</on_right>
        <visible>true</visible>
        <color>
          <color name="Text" red="25" green="25" blue="25">
          </color>
        </color>
      </y_axis>
      <y_axis>
        <title>QL [a.u.]</title>
        <autoscale>true</autoscale>
        <log_scale>true</log_scale>
        <minimum>0.0</minimum>
        <maximum>10.0</maximum>
        <show_grid>true</show_grid>
        <title_font>
          <font family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </title_font>
        <scale_font>
          <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </scale_font>
        <on_right>true</on_right>
        <visible>true</visible>
        <color>
          <color name="Text" red="25" green="25" blue="25">
          </color>
        </color>
      </y_axis>
    </y_axes>
    <traces>
      <trace>
        <name>Detuning</name>
        <x_pv>$(P)$(R)WFDtgQlXAxis</x_pv>
        <y_pv>$(P)$(R)WFCavDetune</y_pv>
        <err_pv></err_pv>
        <axis>0</axis>
        <trace_type>1</trace_type>
        <color>
          <color red="0" green="0" blue="255">
          </color>
        </color>
        <line_width>2</line_width>
        <line_style>0</line_style>
        <point_type>0</point_type>
        <point_size>10</point_size>
        <visible>true</visible>
      </trace>
      <trace>
        <name>QL</name>
        <x_pv>$(P)$(R)WFDtgQlXAxis</x_pv>
        <y_pv>$(P)$(R)WFCavQl</y_pv>
        <err_pv></err_pv>
        <axis>1</axis>
        <trace_type>1</trace_type>
        <color>
          <color red="255" green="0" blue="0">
          </color>
        </color>
        <line_width>2</line_width>
        <line_style>0</line_style>
        <point_type>0</point_type>
        <point_size>10</point_size>
        <visible>true</visible>
      </trace>
    </traces>
  </widget>
  <widget type="xyplot" version="3.0.0">
    <name>X/Y Plot</name>
    <x>400</x>
    <y>120</y>
    <width>360</width>
    <height>280</height>
    <show_legend>false</show_legend>
    <tooltip></tooltip>
    <x_axis>
      <title>Time [ms]</title>
      <autoscale>true</autoscale>
      <log_scale>false</log_scale>
      <minimum>0.0</minimum>
      <maximum>10.0</maximum>
      <show_grid>true</show_grid>
      <title_font>
        <font family="Source Sans Pro" style="REGULAR" size="16.0">
        </font>
      </title_font>
      <scale_font>
        <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
        </font>
      </scale_font>
      <visible>true</visible>
    </x_axis>
    <y_axes>
      <y_axis>
        <title>Phase [degree]</title>
        <autoscale>true</autoscale>
        <log_scale>false</log_scale>
        <minimum>0.0</minimum>
        <maximum>10.0</maximum>
        <show_grid>true</show_grid>
        <title_font>
          <font family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </title_font>
        <scale_font>
          <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </scale_font>
        <on_right>false</on_right>
        <visible>true</visible>
        <color>
          <color name="Text" red="25" green="25" blue="25">
          </color>
        </color>
      </y_axis>
    </y_axes>
    <traces>
      <trace>
        <name></name>
        <x_pv>$(PD)$(RD)Dwn0-XAxis.{"arr":{"s":0,"i":100,"e":-1}}</x_pv>
        <y_pv>$(PD)$(RD)Dwn0-Cmp1.{"arr":{"s":0,"i":100,"e":-1}}</y_pv>
        <err_pv></err_pv>
        <axis>0</axis>
        <trace_type>1</trace_type>
        <color>
          <color red="255" green="0" blue="0">
          </color>
        </color>
        <line_width>2</line_width>
        <line_style>0</line_style>
        <point_type>0</point_type>
        <point_size>10</point_size>
        <visible>true</visible>
      </trace>
    </traces>
  </widget>
  <widget type="xyplot" version="3.0.0">
    <name>X/Y Plot</name>
    <x>400</x>
    <y>450</y>
    <width>359</width>
    <height>280</height>
    <tooltip></tooltip>
    <x_axis>
      <title>Time [ms]</title>
      <autoscale>true</autoscale>
      <log_scale>false</log_scale>
      <minimum>0.0</minimum>
      <maximum>10.0</maximum>
      <show_grid>true</show_grid>
      <title_font>
        <font family="Source Sans Pro" style="REGULAR" size="16.0">
        </font>
      </title_font>
      <scale_font>
        <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
        </font>
      </scale_font>
      <visible>true</visible>
    </x_axis>
    <y_axes>
      <y_axis>
        <title>Phase [degree]</title>
        <autoscale>true</autoscale>
        <log_scale>false</log_scale>
        <minimum>0.0</minimum>
        <maximum>10.0</maximum>
        <show_grid>true</show_grid>
        <title_font>
          <font family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </title_font>
        <scale_font>
          <font name="Default" family="Source Sans Pro" style="REGULAR" size="16.0">
          </font>
        </scale_font>
        <on_right>false</on_right>
        <visible>true</visible>
        <color>
          <color name="Text" red="25" green="25" blue="25">
          </color>
        </color>
      </y_axis>
    </y_axes>
    <traces>
      <trace>
        <name>Phifwd</name>
        <x_pv>$(PD)$(RD)Dwn5-XAxis.{"arr":{"s":0,"i":100,"e":-1}}</x_pv>
        <y_pv>$(PD)$(RD)Dwn5-Cmp1.{"arr":{"s":0,"i":100,"e":-1}}</y_pv>
        <err_pv></err_pv>
        <axis>0</axis>
        <trace_type>1</trace_type>
        <color>
          <color red="0" green="0" blue="255">
          </color>
        </color>
        <line_width>2</line_width>
        <line_style>0</line_style>
        <point_type>0</point_type>
        <point_size>10</point_size>
        <visible>true</visible>
      </trace>
      <trace>
        <name>Phirefl</name>
        <x_pv>$(PD)$(RD)Dwn6-XAxis.{"arr":{"s":0,"i":100,"e":-1}}</x_pv>
        <y_pv>$(PD)$(RD)Dwn6-Cmp1.{"arr":{"s":0,"i":100,"e":-1}}</y_pv>
        <err_pv></err_pv>
        <axis>0</axis>
        <trace_type>1</trace_type>
        <color>
          <color red="255" green="0" blue="0">
          </color>
        </color>
        <line_width>2</line_width>
        <line_style>0</line_style>
        <point_type>0</point_type>
        <point_size>10</point_size>
        <visible>true</visible>
      </trace>
      <trace>
        <name>Phikly</name>
        <x_pv>$(PD)$(RD)Dwn3-XAxis.{"arr":{"s":0,"i":100,"e":-1}}</x_pv>
        <y_pv>$(PD)$(RD)Dwn3-Cmp1.{"arr":{"s":0,"i":100,"e":-1}}</y_pv>
        <err_pv></err_pv>
        <axis>0</axis>
        <trace_type>1</trace_type>
        <color>
          <color red="0" green="128" blue="128">
          </color>
        </color>
        <line_width>2</line_width>
        <line_style>0</line_style>
        <point_type>0</point_type>
        <point_size>10</point_size>
        <visible>true</visible>
      </trace>
    </traces>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Title</name>
    <class>TITLE</class>
    <text>$(C) Conditioning - MB</text>
    <x use_class="true">20</x>
    <y use_class="true">0</y>
    <width>700</width>
    <height use_class="true">50</height>
    <font use_class="true">
      <font name="Header 1" family="Source Sans Pro" style="BOLD_ITALIC" size="36.0">
      </font>
    </font>
    <foreground_color use_class="true">
      <color name="HEADER-TEXT" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <transparent use_class="true">true</transparent>
    <horizontal_alignment use_class="true">0</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
    <wrap_words use_class="true">false</wrap_words>
    <rules>
      <rule name="Label" prop_id="text" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>$(C) conditioning - HB</value>
        </exp>
        <pv_name>$(P)$(R)CavType</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <class>HEADER2</class>
    <text>Cavity Field - AI0</text>
    <x>30</x>
    <y>70</y>
    <width>360</width>
    <height>50</height>
    <font use_class="true">
      <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <class>HEADER2</class>
    <text>Kly/Fwr/Refl Power - AI3/AI5/AI6</text>
    <x>30</x>
    <y>400</y>
    <width>360</width>
    <height>50</height>
    <font use_class="true">
      <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <class>HEADER2</class>
    <text>Cavity Phase - AI0</text>
    <x>400</x>
    <y>70</y>
    <width>360</width>
    <height>50</height>
    <font use_class="true">
      <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <class>HEADER2</class>
    <text>Kly/Fwr/Refl Phase - AI3/AI5/AI6</text>
    <x>400</x>
    <y>400</y>
    <width>360</width>
    <height>50</height>
    <font use_class="true">
      <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <class>HEADER2</class>
    <text>Detuning &amp; QL</text>
    <x>30</x>
    <y>730</y>
    <width>730</width>
    <height>50</height>
    <font use_class="true">
      <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="navtabs" version="2.0.0">
    <name>Navigation Tabs</name>
    <tabs>
      <tab>
        <name>Piezo Controller Status</name>
        <file>piezoController_R.bob</file>
        <macros>
        </macros>
        <group_name></group_name>
      </tab>
      <tab>
        <name>Piezo Controller Settings</name>
        <file>piezoController_W.bob</file>
        <macros>
        </macros>
        <group_name></group_name>
      </tab>
    </tabs>
    <x>790</x>
    <y>510</y>
    <width>570</width>
    <height>210</height>
    <direction>0</direction>
    <tab_width>210</tab_width>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle</name>
    <x>790</x>
    <y>510</y>
    <width>570</width>
    <height>210</height>
    <line_width>2</line_width>
    <line_color>
      <color name="GRAY-BORDER" red="121" green="121" blue="121">
      </color>
    </line_color>
    <transparent>true</transparent>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button</name>
    <actions>
      <action type="open_display">
        <file>/opt/opis/10-Engineer/10-ACC/999-TS2/99-RF/10-Top/PiezoDriver.bob</file>
        <target>window</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text>Piezo Main Window</text>
    <x>1270</x>
    <y>10</y>
    <width>150</width>
    <tooltip>Go to sensor readouts and settings</tooltip>
  </widget>
</display>
